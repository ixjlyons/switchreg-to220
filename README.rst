===============
switchreg-TO220
===============

Description
===========

This is a switching regulator circuit (fixed 5V output) laid out on a board that
is the same size as a TO-220 package. This makes it essentially a drop-in 
replacement for something like a LM7805 with much larger current output
capability and no dropout voltage. This board takes anything from 5V to 15V and
produces 5V at up to 3A.

Acknowledgement must go to Ytai Ben-Tsvi, creator of the `IOIO-OTG`_. This 
circuit is essentially stolen from that board design.

Details, including vendor links and prices are in bill-of-materials.ods.

.. LINKS
.. _`IOIO-OTG`: https://github.com/ytai/ioio/wiki
